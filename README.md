Bird & Associates Psychological Services Inc offers our clients a gentle, compassionate, comprehensive approach to their psychological needs. Through active listening, our Psychologists can help identify what may be contributing to our clients struggles and can identify any barriers that may impede.

Address: 6155 North Street, Suite 303, Halifax, NS B3k 5R3, Canada

Phone: 902-480-8880

Website: http://birdpsychological.com
